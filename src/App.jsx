import React from "react";
import './App.css';
import PropTypes from "prop-types";
import Modal from "./components/Modal/Modal";
import axios from "axios";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import BooksMain from "./components/BooksMain/BooksMain";

class App extends React.Component {
    static propTypes = {
        basket: PropTypes.number,
        wishList: PropTypes.number,
    };

    static defaultProps = {
        basket: 0,
        wishList: 0,
    };

    state = {
        openBuyModal: "false",
        openWishListModal: "false",
        books: [],
        basket: 0,
        wishList: 0,
        arrWishList: []
    }

    componentDidMount() {
        this.setState({
            ...this.state,
            basket: localStorage.getItem('basket') ? parseInt(localStorage.getItem('basket')) : 0,
            wishList: localStorage.getItem('wishList') ? parseInt(localStorage.getItem('wishList')) : 0
        })
        axios.get("/books.json").then(res => {
            this.setState({
                ...this.state,
                books: [
                    ...res.data
                ]
            })
        })
    }

    render() {
        const confirmOrder = () => {

            localStorage.setItem('basket', this.state.basket + 1);
            this.setState({
                ...this.state,
                openBuyModal: "false",
                basket: this.state.basket + 1
            })

        }
        const confirmWishList = () => {
            localStorage.setItem('wishList', this.state.wishList + 1);
            const idCandidate = localStorage.getItem('wishCandidate');
            console.log(idCandidate)
            this.setState({
                ...this.state,
                openWishListModal: "false",
                wishList: this.state.wishList + 1,
                arrWishList: [
                    ...this.state.arrWishList,
                    idCandidate
                ]
            })
        }
        const handleClickToBuy = () => {
            this.setState({
                ...this.state,
                openBuyModal: this.state.openBuyModal === "false" ? "true" : "false"
            })
        }
        const handleClickWishList = () => {
            this.setState({
                ...this.state,
                openWishListModal: this.state.openWishListModal === "false" ? "true" : "false"
            })
        }
        const texts = {
            headerFirst: "Увага!",
            headerSecond: "Увага!",
            bodyFirst: "Зверніть увагу, що товар поверненню не підлягає",
            bodySecond: "Ви впевнені, що хочете додати даний товар до обраних?"
        }
        return (
            <>
                <Header basket={this.state.basket} wishList={this.state.wishList}/>
                <main className="main">
                    <BooksMain
                        books={this.state.books}
                        handleClickToBuy={handleClickToBuy}
                        handleClickWishList={handleClickWishList}
                        arrWishList={this.state.arrWishList}
                    />
                </main>
                <Footer/>
                {this.state.openBuyModal === "true" ?
                    <Modal handleClick={handleClickToBuy} confirm={confirmOrder} text1={texts.headerFirst}
                           text2={texts.bodyFirst} theme="danger" isClosed={true}/> : null}
                {this.state.openWishListModal === "true" ?
                    <Modal handleClick={handleClickWishList} confirm={confirmWishList} text1={texts.headerSecond}
                           text2={texts.bodySecond} theme="primary" isClosed={true}/> : null}
            </>
        )
    }
}

export default App;
