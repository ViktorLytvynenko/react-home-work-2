import footerLogo from "../../img/footer-logo.svg"
import style from "./Footer.module.scss"

const Footer = () => {
    const linkStore = ["Інформація", "Каталог", "Новини", "Види оплати"];
    const LinkCollection = linkStore.map((linkItem, index) =>
        <a href="#" key={index} className={style.footer_container_nav}>{linkItem}</a>)
    return (
        <footer className={style.footer}>
            <div className={style.footer_container}>
                <a href="#" className={style.footer_container_c1}>
                    <img className={style.footer_container_c1_img} src={footerLogo} alt="footer logo"
                         title="footer logo"/>
                </a>
                {LinkCollection}
            </div>
            <hr/>
                <p className={style.footer_copyright}>
                    © 2003 — 2023 Всі права захищені. Використання матеріалів сайту можливе тільки з письмового дозволу
                    компанії.
                </p>
        </footer>
    )
}

export default Footer